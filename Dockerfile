FROM nginx:stable

COPY ./$CI_PROJECT_DIR/ /usr/share/nginx/html/

RUN ls -laR /usr/share/nginx/html/*